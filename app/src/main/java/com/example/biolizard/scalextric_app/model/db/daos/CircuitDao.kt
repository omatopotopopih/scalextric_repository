package com.example.biolizard.scalextric_app.model.db.daos

import com.example.biolizard.scalextric_app.model.db.entities.EntityCircuit
import android.arch.persistence.room.*


@Dao
interface CircuitDao {

    @Insert
    fun insertCircuits(entityCircuits: List<EntityCircuit>)

    @Query("DELETE FROM CIRCUIT_TABLE")
    fun deleteCircuits()

    @Delete
    fun deleteCircuits(entityCircuits: List<EntityCircuit>)

    @Update
    fun updateCircuits(entityCircuits: List<EntityCircuit>)

    @Query("SELECT * from circuit_table ORDER BY id ASC")
    fun getAllCircuits(): List<EntityCircuit>

}