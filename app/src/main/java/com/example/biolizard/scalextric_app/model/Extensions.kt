package com.example.biolizard.scalextric_app.model

import com.example.biolizard.scalextric_app.model.local.CategoryEnum
import com.example.biolizard.scalextric_app.model.local.Manufacter
import com.example.biolizard.scalextric_app.model.local.Product

fun List<Product>.manufacterFilter(manufacters: Array<Manufacter>) = this.filter { it.manufacter in manufacters }

fun List<Product>.categoryFilter(category: CategoryEnum) = this.filter {it.category.categoryType == category}