package com.example.biolizard.scalextric_app.model.local

import android.os.Parcelable

interface Product: Parcelable{
    var id:Int?
    var name: String
    var brand: String
    var price: Double
    var manufacter: Manufacter
    var description: String
    var photo: Int
    var category: Category
}