package com.example.biolizard.scalextric_app.model.db

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.example.biolizard.scalextric_app.model.db.daos.*
import android.arch.persistence.room.Room
import android.arch.persistence.room.TypeConverters
import android.os.AsyncTask
import com.example.biolizard.scalextric_app.BuildConfig
import com.example.biolizard.scalextric_app.R
import com.example.biolizard.scalextric_app.model.db.converters.ConverterCategory
import com.example.biolizard.scalextric_app.model.db.converters.ConverterManufacter
import com.example.biolizard.scalextric_app.model.db.entities.*
import com.example.biolizard.scalextric_app.model.local.CategoryEnum
import com.example.biolizard.scalextric_app.model.local.Manufacter

@Database(
    entities = [EntityCircuit::class, EntityCar::class, EntityAccesory::class, CircuitToCar::class, CircuitToAccesory::class],
    version = 1
)
@TypeConverters(ConverterManufacter::class, ConverterCategory::class)
abstract class DB : RoomDatabase() {

    abstract val accesoryDao: AccesoryDao
    abstract val carDao: CarDao
    abstract val circuitDao: CircuitDao
    abstract val circuitToAccesoryDao: CircuitToAccesoryDao
    abstract val circuitToCarDao: CircuitToCarDao

    companion object {

        private val DB_NAME = BuildConfig.DATABASE_NAME
        @Volatile
        var INSTANCE: DB? = null


        fun getDatabase(context: Context): DB? {
            INSTANCE ?: synchronized(DB::class.java) {
                INSTANCE ?: Room.databaseBuilder(context, DB::class.java, DB_NAME)
                    .fallbackToDestructiveMigration()
                    .addCallback(object : Callback() {
                        override fun onCreate(db: SupportSQLiteDatabase) {
                            super.onCreate(db)
                            AccesoryDaoAsyncTask(INSTANCE!!)
                                .execute()
                            CarDaoAsyncTask(INSTANCE!!)
                                .execute()
                            CircuitDaoAsyncTask(INSTANCE!!)
                                .execute()
                            CircuitToCarDaoAsayncTask(INSTANCE!!)
                                .execute()
                            CircuitToAccesoryDaoAsayncTask(INSTANCE!!)
                                .execute()
                        }

                    }).build().also { INSTANCE = it }
            }

            return INSTANCE
        }

        class AccesoryDaoAsyncTask(db: DB) : AsyncTask<Void, Void, Void>() {

            var mAsyncTaskAccesoryDao: AccesoryDao = db.accesoryDao

            override fun doInBackground(vararg params: Void?): Void? {
                val entityAccesories: ArrayList<EntityAccesory> = ArrayList()
                entityAccesories.addAll(
                    arrayOf(
                        EntityAccesory(
                            "Curva super descilante",
                            "Ninco",
                            10.0,
                            "Una curva super deslizante",
                            Manufacter.NINCO,
                            R.drawable.curvasuperdeslizantescalextric,
                            CategoryEnum.RALLY
                        ),
                        EntityAccesory(
                            "Farolas Scalextric",
                            "Tecnitoys",
                            7.0,
                            "Farolas para iluminar tu pista",
                            Manufacter.TECNITOYS,
                            R.drawable.farolas,
                            CategoryEnum.DTM
                        ),
                        EntityAccesory(
                            "Pistas de STS",
                            "Carrera",
                            15.0,
                            "Trozo de pista rudimentario apedreado.",
                            Manufacter.CARRERA,
                            R.drawable.sts_pista,
                            CategoryEnum.F1
                        ),
                        EntityAccesory(
                            "Curvas Cambio de Carril",
                            "Fly",
                            12.0,
                            "Unas curvas bien solidas perfectas para cambios.",
                            Manufacter.FLY,
                            R.drawable.cambio_de_carril_curva,
                            CategoryEnum.RALLY
                        ),
                        EntityAccesory(
                            "Trafo y Mandos Jack",
                            "Hornby",
                            20.0,
                            "Mandos y un trafo para controlar tus vehiculos.",
                            Manufacter.HORNBY,
                            R.drawable.trafo_mandos,
                            CategoryEnum.DTM
                        )

                        ,
                        EntityAccesory(
                            "SCX Cuentavueltas",
                            "Exin",
                            18.0,
                            "Cuentavueltas con límite de 99 vueltas.",
                            Manufacter.EXIN,
                            R.drawable.cuentavueltas,
                            CategoryEnum.F1
                        )
                    )
                )
                mAsyncTaskAccesoryDao.insertAccesories(entityAccesories)
                return null
            }

        }


        class CarDaoAsyncTask(db: DB) : AsyncTask<Void, Void, Void>() {

            var mAsyncTaskAccesoryDao = db.carDao

            override fun doInBackground(vararg params: Void?): Void? {
                val entityCars: ArrayList<EntityCar> = arrayListOf(
                    EntityCar(
                        "March GP Canada",
                        "Hans Stuck",
                        200.0,
                        "Un coche March",
                        Manufacter.FLY,
                        R.drawable.flymarch,
                        CategoryEnum.F1
                    ), EntityCar(
                        "Mercedes CLK D2",
                        "Mercedes",
                        100.0,
                        "Un Mercedes CLK D2",
                        Manufacter.NINCO,
                        R.drawable.mercedesclk,
                        CategoryEnum.DTM
                    ), EntityCar(
                        "Ford XY",
                        "Ford",
                        150.0,
                        "El Ford modelo XY",
                        Manufacter.HORNBY,
                        R.drawable.fordxy1,
                        CategoryEnum.RALLY
                    ),EntityCar(
                        "BMW slot Carrera",
                        "BMW",
                        100.0,
                        "Blanco y azul, Toma bien las curvas.",
                        Manufacter.CARRERA,
                        R.drawable.slot_bmw_carrera,
                        CategoryEnum.DTM
                    ), EntityCar(
                        "GT1 Audi",
                        "Audi",
                        120.0,
                        "Amarillo chillón pero elegante.",
                        Manufacter.TECNITOYS,
                        R.drawable.gt1_tecnitoys,
                        CategoryEnum.F1
                    ), EntityCar(
                        "Carrera Ferrari F1",
                        "Ferrari",
                        85.0,
                        "Ferrari Rojo como el de Fernando Alonso.",
                        Manufacter.CARRERA,
                        R.drawable.ferrari_f1,
                        CategoryEnum.F1
                    ), EntityCar(
                        "Ford Focus Azul",
                        "Ford",
                        110.0,
                        "Perfecto para circuitos de Rally Dakar",
                        Manufacter.NINCO,
                        R.drawable.ford_rally,
                        CategoryEnum.RALLY
                    )

                )
                mAsyncTaskAccesoryDao.insertCars(entityCars)
                return null
            }

        }


        class CircuitDaoAsyncTask(db: DB) : AsyncTask<Void, Void, Void>() {

            var mAsyncTaskAccesoryDao = db.circuitDao

            override fun doInBackground(vararg params: Void?): Void? {
                val entityCircuits: ArrayList<EntityCircuit> = ArrayList()
                entityCircuits.addAll(
                    arrayListOf(
                        EntityCircuit(
                            "Digital 132",
                            "Digital",
                            250.0,
                            "Circuito digital del uno al tres",
                            Manufacter.NINCO,
                            R.drawable.digital132,
                            CategoryEnum.DTM
                        ), EntityCircuit(
                            "Eurocup megane",
                            "Megane",
                            550.50,
                            "Circuito de eurocup",
                            Manufacter.CARRERA,
                            R.drawable.eurocupmegane,
                            CategoryEnum.RALLY
                        ), EntityCircuit(
                            "Copa Peugot",
                            "Peugot",
                            430.0,
                            "Circuito de copa Peugot Ligero",

                            Manufacter.TECNITOYS,
                            R.drawable.copatecni,
                            CategoryEnum.RALLY
                        ),EntityCircuit(
                            "Circuito C1 Formula 1 Maclaren",
                            "Maclaren",
                            280.0,
                            "Un circuito C1 dedicado exclusivamente a F1.",
                            Manufacter.EXIN,
                            R.drawable.c1_maclaren,
                            CategoryEnum.F1
                        ), EntityCircuit(
                            "Rally circuit X-TREME Classic",
                            "Megane",
                            500.0,
                            "Circuito rally X-TREME para 2 personas.",
                            Manufacter.FLY,
                            R.drawable.rally_xtreme,
                            CategoryEnum.RALLY
                        ), EntityCircuit(
                            "C2 Formula 1 Maclaren",
                            "Maclaren",
                            270.0,
                            "Un circuito C2 dedicado exclusivamente a F1.",
                            Manufacter.HORNBY,
                            R.drawable.c2_maclaren,
                            CategoryEnum.F1
                        )
                    )
                )
                mAsyncTaskAccesoryDao.insertCircuits(entityCircuits)
                return null
            }

        }

        class CircuitToCarDaoAsayncTask(db: DB) : AsyncTask<Void, Void, Void>() {

            var mAsyncCircuitToCarDao = db.circuitToCarDao

            override fun doInBackground(vararg params: Void?): Void? {
                val entityNM: ArrayList<CircuitToCar> = ArrayList()
                val data = CircuitToCar()
                data.carId = 2
                data.circuitId = 1
                entityNM.add(data)
                mAsyncCircuitToCarDao.insertCircuitToCar(entityNM)
                return null
            }

        }

        class CircuitToAccesoryDaoAsayncTask(db: DB) : AsyncTask<Void, Void, Void>() {

            var mAsyncCircuitToCarDao = db.circuitToAccesoryDao

            override fun doInBackground(vararg params: Void?): Void? {
                val entityNM: ArrayList<CircuitToAccesory> = ArrayList()
                val data = CircuitToAccesory()
                data.accesoryId = 1
                data.circuitId = 1
                entityNM.add(data)
                mAsyncCircuitToCarDao.insertCircuitToAccesory(entityNM)
                return null
            }

        }
    }
}