package com.example.biolizard.scalextric_app.activities.activities

import android.animation.Animator
import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AlertDialog
import android.util.Log
import com.example.biolizard.scalextric_app.R
import android.view.View
import com.example.biolizard.scalextric_app.fragments.*
import com.example.biolizard.scalextric_app.model.categoryFilter
import com.example.biolizard.scalextric_app.model.db.repository.Repository
import com.example.biolizard.scalextric_app.model.local.*
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity(),
    MainFragment.OnMainFragmentInteractionListener,
    ShowProductsFragment.OnShowFragmentInteractionListener, NewsFragment.OnNewsFragmentInteractionListener,
    HomeFragment.OnHomeFragmentInteractionListener {

    private var items: ArrayList<Product> = ArrayList()
    private var itemsFiltered: ArrayList<Product> = ArrayList()
    private var itemsClass: ArrayList<Product> = ArrayList()
    lateinit var product: Product

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getData()
        loadLocate()
        setContentView(R.layout.activity_main)
        showHomeFragment()
        createPrincipalTab()
        sethomeButtonFunction()
        setlanguageButtonFunction()
        setShoppingCartButtonFucntion()
        createSecundaryTab()
        animation_view.setAnimation("blue_car_animation.json")
        animation_view.playAnimation()
        animation_view.speed
        setAnimationListener()

    }
    // Funcion para setear el listener de la animacion
    fun setAnimationListener(){
        animation_view.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {
            }

            override fun onAnimationEnd(animation: Animator) {

            }

            override fun onAnimationCancel(animation: Animator) {
            }

            override fun onAnimationRepeat(animation: Animator) {
                Log.e("Animation:", "repeat")
                animation_view.visibility = View.GONE
                animation_view.cancelAnimation()
            }
        })
    }

    // Funcion que se baja todos los datos de la base de datos al arrancar la aplicacion
    fun getData() {
        doAsync {
            val rep = Repository(this@MainActivity.application)
            items.addAll(rep.getAllCars())
            items.addAll(rep.getAllAccesory())
            items.addAll(rep.getAllCircuits())
            Log.v("bbdd", items.size.toString())
        }
    }

    // Listener del homebutton
    fun sethomeButtonFunction() {
        home_button.setOnClickListener {
            showHomeFragment()
        }
    }

    // Listener del boton de cambiar idioma
    fun setlanguageButtonFunction() {
        language_button.setOnClickListener {
            showChangeLanguage()
        }
    }

    // listener del boton del carrito
    fun setShoppingCartButtonFucntion() {
        fab_shopping_cart.setOnClickListener {
            val fm = supportFragmentManager
            val cartDFragment = ShoppingCartDialogFragment()
            cartDFragment.show(fm, "Dialog Fragment")
        }
    }

    // Menu principal, es decir, el donde se muestran las categorias
    fun createPrincipalTab() {
        tabs.tabGravity = TabLayout.GRAVITY_FILL
        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {
                p0?.let {
                    when (it.position) {
                        0 -> {
                            showMainFragment(it.position)
                        }
                        1 -> {
                            showMainFragment(it.position)
                        }
                        2 -> {
                            showMainFragment(it.position)
                        }
                        3 -> {
                            showBrandsFragment()
                        }
                        4 -> {
                            showNewsFragment(items)
                        }
                    }
                }

            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                p0?.let {
                    when (it.position) {
                        0 -> {
                            showMainFragment(it.position)
                        }
                        1 -> {
                            showMainFragment(it.position)
                        }
                        2 -> {
                            showMainFragment(it.position)
                        }
                        3 -> {
                            showBrandsFragment()
                        }
                        4 -> {
                            showNewsFragment(items)
                        }
                    }
                }
            }
        })
    }

    // Menu secundario, es decir, el donde se muestran los productos ya filtrados por categoria y  subcategorias
    fun createSecundaryTab() {

        tab_secundary.tabGravity = TabLayout.GRAVITY_FILL
        tab_secundary.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabReselected(p0: TabLayout.Tab?) {
                p0?.let {
                    if (itemsClass.isEmpty()) {
                        itemsClass = items
                    }
                    when (it.position) {
                        //RALLY
                        0 -> {
                            itemsFiltered = ArrayList(itemsClass.categoryFilter(CategoryEnum.RALLY))
                        }
                        //DTM
                        1 -> {

                            itemsFiltered = ArrayList(itemsClass.categoryFilter(CategoryEnum.DTM))
                        }
                        //F1
                        2 -> {
                            itemsFiltered = ArrayList(itemsClass.categoryFilter(CategoryEnum.F1))
                        }
                    }
                    showProductsFragment(itemsFiltered)
                }
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {

            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                p0?.let {
                    if (itemsClass.isEmpty()) {
                        itemsClass = items
                    }
                    when (it.position) {
                        //RALLY
                        0 -> {
                            itemsFiltered = ArrayList(itemsClass.categoryFilter(CategoryEnum.RALLY))
                        }
                        //DTM
                        1 -> {

                            itemsFiltered = ArrayList(itemsClass.categoryFilter(CategoryEnum.DTM))
                        }
                        //F1
                        2 -> {
                            itemsFiltered = ArrayList(itemsClass.categoryFilter(CategoryEnum.F1))
                        }
                    }
                    showProductsFragment(itemsFiltered)

                }

            }
        })


    }

    // onClick del fragment de las categorias
    override fun onFragmentInteraction(item: Category, usedClass: Int) {
        Log.d("Used class", usedClass.toString())
        itemsClass = when (usedClass) {
            0 -> ArrayList(items.filterIsInstance<Car>())

            1 -> ArrayList(items.filterIsInstance<Circuit>())

            2 -> ArrayList(items.filterIsInstance<Accesory>())

            else -> ArrayList(items.filterIsInstance<Car>())
        }

        when (item.categoryType) {
            CategoryEnum.DTM -> {
                tab_secundary.getTabAt(1)?.select()
            }
            CategoryEnum.F1 -> {
                tab_secundary.getTabAt(2)?.select()
            }
            CategoryEnum.RALLY -> {
                tab_secundary.getTabAt(0)?.select()
            }
        }
    }

    // onClick del fragment de los productos
    override fun onShowFragmentInteraction(item: Product) {
        val bundle = Bundle()
        bundle.putParcelable("product", item)
        ShowDetailsDialogFragment.newInstance(item).apply {
            showNow(supportFragmentManager, "tag")
            arguments = bundle
        }

    }

    // onClick del fragment de las novedades
    override fun onNewsFragmentInteraction(item: Product) {
        val bundle = Bundle()
        bundle.putParcelable("product", item)
        ShowDetailsDialogFragment.newInstance(item).apply {
            showNow(supportFragmentManager, "tag")
            arguments = bundle
        }
    }

    override fun onHomeFragmentInteractionListener(uri: Uri) {}


    // Funcion que muestra el fragment con los productos
    private fun showProductsFragment(sortedProducts: ArrayList<Product>) {
        tab_secundary.visibility = View.VISIBLE
        val bundle = Bundle()
        bundle.putParcelableArrayList("secondMenuItem", sortedProducts)
        supportFragmentManager.beginTransaction().also { fragmentTransaction ->
            ShowProductsFragment.newInstance(2, "Circuitos").apply {
                arguments = bundle
                fragmentTransaction.replace(R.id.fragmentMainLayout, this)
                fragmentTransaction.addToBackStack(null)
            }
        }.commit()
    }

    // Funcion que muestra el fragment con las marcas para filtrar
    private fun showBrandsFragment() {
        supportFragmentManager.beginTransaction().apply {
            tab_secundary.visibility = View.GONE
            replace(R.id.fragmentMainLayout, BrandsFragment.newInstance(2, "Circuitos"))
            addToBackStack(null)
        }.commit()
    }

    // Funcion que muestra el fragment de home
    private fun showHomeFragment() {

        supportFragmentManager.beginTransaction().apply {
            tab_secundary.visibility = View.GONE
            replace(R.id.fragmentMainLayout, HomeFragment.newInstance(1, "home"))
            addToBackStack(null)
        }.commit()
    }

    // Funcion que muestra el fragment con las categorias
    private fun showMainFragment(menuItemClicked: Int) {
        tab_secundary.visibility = View.GONE
        val bundle = Bundle()
        bundle.putInt("menuItemClicked", menuItemClicked)
        supportFragmentManager.beginTransaction().also { fragmentTransaction ->
            MainFragment.newInstance(2, "MainFragment").apply {
                arguments = bundle
                fragmentTransaction.replace(R.id.fragmentMainLayout, this)
                fragmentTransaction.addToBackStack(null)
            }
        }.commit()
    }

    // Funcion que muestra el fragment con las novedades
    private fun showNewsFragment(newestProducts: ArrayList<Product>) {
        tab_secundary.visibility = View.GONE
        val bundle = Bundle()
        bundle.putParcelableArrayList("news", newestProducts)
        supportFragmentManager.beginTransaction().also { fragmentTransaction ->
            NewsFragment.newInstance(2, "news").apply {
                arguments = bundle
                fragmentTransaction.replace(R.id.fragmentMainLayout, this)
                fragmentTransaction.addToBackStack(null)
            }
        }.commit()
    }

    // Funcion para cambiar la aplicacion de idioma
    private fun showChangeLanguage() {
        val listLanguages = arrayOf("English", "French", "Spanish")

        val mBuilder = AlertDialog.Builder(this)
        mBuilder.setTitle("Choose Language")
        mBuilder.setSingleChoiceItems(listLanguages, -1) { dialog, which ->
            when (which) {
                0 -> {
                    setLocate("en")
                    recreate()
                }
                1 -> {
                    setLocate("fr")
                    recreate()
                }
                2 -> {
                    setLocate("es")
                    recreate()
                }
            }
            dialog.dismiss()
        }

        val mDialog = mBuilder.create()
        mDialog.show()
    }

    // Funcion que recibe el idioma
    private fun setLocate(Lang: String) {

        val locale = Locale(Lang)

        Locale.setDefault(locale)

        val config = Configuration()



        config.locale = locale
        baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)

        val editor = getSharedPreferences("Settings", Context.MODE_PRIVATE).edit()
        editor.putString("My_Lang", Lang)
        editor.apply()
    }

    // Funcion que carga el nuevo idioma
    private fun loadLocate() {
        val sharedPreferences = getSharedPreferences("Settings", Activity.MODE_PRIVATE)
        sharedPreferences.getString("My_Lang", "")?.let {
            setLocate(it)
        }
    }
}