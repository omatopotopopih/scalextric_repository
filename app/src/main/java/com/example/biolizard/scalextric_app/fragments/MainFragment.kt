package com.example.biolizard.scalextric_app.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.biolizard.scalextric_app.R
import com.example.biolizard.scalextric_app.adapters.MainListAdapter
import android.R.attr.defaultValue
import android.util.Log
import com.example.biolizard.scalextric_app.model.local.Car
import com.example.biolizard.scalextric_app.model.local.Category
import com.example.biolizard.scalextric_app.model.local.CategoryEnum
import com.example.biolizard.scalextric_app.model.local.Product
import java.util.*


class MainFragment : Fragment() {
    interface OnMainFragmentInteractionListener {
        fun onFragmentInteraction(item: Category, usedClass: Int)
    }
    lateinit var product: Product
    private var items: ArrayList<Category> = ArrayList()
    private var mListener: OnMainFragmentInteractionListener? = null
    private var data = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        this.arguments?.let {
            data = it.getInt("menuItemClicked")
        }
        return inflater.inflate(R.layout.fragment_main, container, false).apply {
            findViewById<RecyclerView>(R.id.recyclerView_main_list).apply {
                layoutManager = GridLayoutManager(this.context, 2)
                items.addAll(
                    arrayOf(
                        Category(CategoryEnum.F1),
                        Category(CategoryEnum.RALLY),
                        Category(CategoryEnum.DTM)
                    )
                )
                adapter = MainListAdapter(items, mListItemListener)

            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnMainFragmentInteractionListener) {
            mListener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    val mListItemListener = object : MainListAdapter.OnCellItemClickListener {
        override fun onItemClick(item: Category) {
            mListener?.onFragmentInteraction(item, data)
        }
    }

    // Funcion para instanciar el fragment
    companion object {
        fun newInstance(page: Int, title: String): MainFragment {
            val mainFragment = MainFragment()
            val args = Bundle()
            args.putInt("someInt", page)
            args.putString("someTitle", title)
            mainFragment.arguments = args
            return mainFragment
        }
    }
}