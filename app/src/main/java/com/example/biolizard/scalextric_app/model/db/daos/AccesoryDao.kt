package com.example.biolizard.scalextric_app.model.db.daos

import com.example.biolizard.scalextric_app.model.db.entities.EntityAccesory
import android.arch.persistence.room.*


@Dao
interface AccesoryDao {

    @Insert
    fun insertAccesories(entityAccesories: List<EntityAccesory>)

    @Query("DELETE FROM ACCESORY_TABLE")
    fun deleteAccesories()

    @Delete
    fun deleteAccesories(entityAccesories: List<EntityAccesory>)

    @Update
    fun updateAccesories(entityAccesories: List<EntityAccesory>)

    @Query("SELECT * from accesory_table ORDER BY id ASC")
    fun getAllAccesories(): List<EntityAccesory>

    @Query("SELECT * from accesory_table WHERE id = :accesoryId LIMIT 1")
    fun getAccesoryFromId(accesoryId: Int): EntityAccesory

}