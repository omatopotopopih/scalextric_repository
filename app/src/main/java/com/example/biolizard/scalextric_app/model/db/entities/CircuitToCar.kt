package com.example.biolizard.scalextric_app.model.db.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Index


@Entity(
    tableName = "circuit_to_car_table",
    primaryKeys = ["carId", "circuitId"],
    foreignKeys = [ForeignKey(
        entity = EntityCircuit::class,
        parentColumns = ["id"],
        childColumns = ["circuitId"],
        onDelete = ForeignKey.CASCADE
    ), ForeignKey(
        entity = EntityCar::class,
        parentColumns = ["id"],
        childColumns = ["carId"],
        onDelete = ForeignKey.CASCADE
    )],
    indices= [
        Index(value = ["carId"]),
        Index(value = ["circuitId"])
    ]
)
class CircuitToCar {
    var carId: Int = 0
    var circuitId: Int = 0
}