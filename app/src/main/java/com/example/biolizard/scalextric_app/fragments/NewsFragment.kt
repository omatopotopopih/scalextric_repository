package com.example.biolizard.scalextric_app.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.biolizard.scalextric_app.R
import com.example.biolizard.scalextric_app.adapters.NewsListAdapter
import android.support.v7.widget.LinearLayoutManager
import com.example.biolizard.scalextric_app.model.local.Product


class NewsFragment : Fragment() {
    interface OnNewsFragmentInteractionListener {
        fun onNewsFragmentInteraction(item: Product)
    }
    private var items: ArrayList<Product> = ArrayList()
    private var listenerNews: OnNewsFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle = this.arguments
        // Recibimos la lista de novedades por argumentos
        bundle?.let {
            items = it.getParcelableArrayList("news")

        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news, container, false).apply {
            findViewById<RecyclerView>(R.id.recyclerView_news_list).apply {
                layoutManager = LinearLayoutManager(this.context).apply {
                    orientation = LinearLayoutManager.HORIZONTAL
                }
                adapter = NewsListAdapter(items, mListItemListener)
            }
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnNewsFragmentInteractionListener) {
            listenerNews = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        listenerNews = null
    }
    val mListItemListener = object : NewsListAdapter.OnNewsCellItemClickListener{
        override fun onItemClick(item: Product) {
            listenerNews?.onNewsFragmentInteraction(item)
        }
    }
    // Funcion para instanciar el fragment
    companion object {
        fun newInstance(page: Int, title: String): NewsFragment {
            val newsFragment = NewsFragment()
            val args = Bundle()
            args.putInt("someInt", page)
            args.putString("someTitle", title)
            newsFragment.arguments = args
            return newsFragment
        }
    }
}
