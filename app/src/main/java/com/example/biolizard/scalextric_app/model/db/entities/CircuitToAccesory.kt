package com.example.biolizard.scalextric_app.model.db.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Index

@Entity(
    tableName = "circuit_to_accesory_table",
    primaryKeys = ["circuitId", "accesoryId"],
    foreignKeys = [ForeignKey(
        entity = EntityCircuit::class,
        parentColumns = ["id"],
        childColumns = ["circuitId"],
        onDelete = ForeignKey.CASCADE
    ), ForeignKey(
        entity = EntityAccesory::class,
        parentColumns = ["id"],
        childColumns = ["accesoryId"],
        onDelete = ForeignKey.CASCADE
    )],
    indices= [
        Index(value = ["accesoryId"]),
        Index(value = ["circuitId"])
    ]
)
class CircuitToAccesory {
    var accesoryId: Int = 0
    var circuitId: Int = 0
}