package com.example.biolizard.scalextric_app.model.local

object Cart {
    //Hash map to represent the ammount of products (As the value) and the product (As the key)
    var selectedProducts = LinkedHashMap<Product, Int>()

    fun getTotalPrice(): Double{
        var result = 0.0
        selectedProducts.forEach{
            result += it.key.price * it.value
        }
        return result
    }


}