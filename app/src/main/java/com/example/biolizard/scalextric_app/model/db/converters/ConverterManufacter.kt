package com.example.biolizard.scalextric_app.model.db.converters

import android.arch.persistence.room.TypeConverter
import com.example.biolizard.scalextric_app.model.local.Manufacter
import com.google.gson.Gson

object ConverterManufacter {


    @TypeConverter
    @JvmStatic
    fun manufacterToString(manufacter: Manufacter): String {
        return Gson().toJson(manufacter)
    }


    @TypeConverter
    @JvmStatic
    fun stringToManufacter(manufacter: String): Manufacter? =
        Gson().fromJson(manufacter, Manufacter::class.java)

}