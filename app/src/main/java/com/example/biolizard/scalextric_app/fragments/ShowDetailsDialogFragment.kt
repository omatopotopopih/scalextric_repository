package com.example.biolizard.scalextric_app.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.biolizard.scalextric_app.R
import com.example.biolizard.scalextric_app.model.local.Cart
import com.example.biolizard.scalextric_app.model.local.Product
import kotlinx.android.synthetic.main.show_details_dialog_fragment.view.*

class ShowDetailsDialogFragment : DialogFragment() {
    lateinit var product: Product

    companion object {
        fun newInstance(item: Product) = ShowDetailsDialogFragment().apply {
            arguments = Bundle().apply {
                putParcelable("product", item)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            product = it.getParcelable("product")
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.show_details_dialog_fragment, container, false)
        v.btn_close.setOnClickListener {
            dialog.dismiss()
        }
        v.details_name.text = product.name
        v.details_description.text = product.description
        v.details_price.text = product.price.toString() + getString(R.string.stringEuro)
        v.image_details.setImageResource(product.photo)

        v.btn_add_cart.setOnClickListener {
            if(Cart.selectedProducts[product] != null){
            Cart.selectedProducts[product]?.plus(1)
            }else{
                Cart.selectedProducts[product] = 1
            }

                Toast.makeText(
                    view?.context,
                    "Se ha añadido"  + product.name + " al carrito.",
                    Toast.LENGTH_LONG
                ).show()
        }
        return v
    }

}