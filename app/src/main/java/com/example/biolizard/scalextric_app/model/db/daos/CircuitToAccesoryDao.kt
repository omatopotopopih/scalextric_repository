package com.example.biolizard.scalextric_app.model.db.daos

import android.arch.persistence.room.*
import com.example.biolizard.scalextric_app.model.db.entities.CircuitToAccesory

@Dao
interface CircuitToAccesoryDao {

    @Insert
    fun insertCircuitToAccesory(data: List<CircuitToAccesory>)

    @Delete
    fun deleteCircuitToAccesory(entityAccesories: List<CircuitToAccesory>)

    @Update
    fun updateCircuitToAccesory(entityAccesories: List<CircuitToAccesory>)

    @Query("SELECT * from CIRCUIT_TO_ACCESORY_TABLE WHERE circuitId = :id")
    fun getAllAccesoriesFromCircuit(id:Int): List<CircuitToAccesory>

    @Query("SELECT * from CIRCUIT_TO_ACCESORY_TABLE WHERE accesoryId = :id")
    fun getAllCircuitsFromAccesory(id:Int): List<CircuitToAccesory>
}