package com.example.biolizard.scalextric_app.model.local

import android.os.Parcel
import android.os.Parcelable
import com.example.biolizard.scalextric_app.model.db.entities.EntityAccesory


class Accesory(
    override var id: Int? = null,
    override var name: String,
    override var brand: String,
    override var price: Double,
    override var description: String,
    override var manufacter: Manufacter,
    override var photo: Int,
    override var category: Category
) : Product {

    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readDouble(),
        parcel.readString(),
        Manufacter.valueOf(parcel.readString()),
        parcel.readInt(),
        Category(CategoryEnum.valueOf(parcel.readString()))
    )

    constructor(entity: EntityAccesory) : this(
        entity.id,
        entity.name,
        entity.brand,
        entity.price,
        entity.description,
        entity.manufacter,
        entity.photo,
        Category(entity.category)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(name)
        parcel.writeString(brand)
        parcel.writeDouble(price)
        parcel.writeString(description)
        parcel.writeInt(photo)
        parcel.writeString(manufacter.name)
        parcel.writeString(category.categoryType.name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Accesory> {
        override fun createFromParcel(parcel: Parcel): Accesory {
            return Accesory(parcel)
        }

        override fun newArray(size: Int): Array<Accesory?> {
            return arrayOfNulls(size)
        }
    }

}