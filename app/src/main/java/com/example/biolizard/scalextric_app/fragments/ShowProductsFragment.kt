package com.example.biolizard.scalextric_app.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.biolizard.scalextric_app.R
import com.example.biolizard.scalextric_app.adapters.ShowProductsListAdapter
import com.example.biolizard.scalextric_app.model.local.Product
import kotlinx.android.synthetic.main.fragment_show_products.*


class ShowProductsFragment : Fragment() {
    interface OnShowFragmentInteractionListener {
        fun onShowFragmentInteraction(item: Product)
    }
    private var listener: OnShowFragmentInteractionListener? = null
    private lateinit var recyclerView: RecyclerView
    private var items: ArrayList<Product> = ArrayList()
    private lateinit var showProductsListAdapter: ShowProductsListAdapter
    lateinit var product: Product


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        showProductsListAdapter = ShowProductsListAdapter(items,mListItemListener)

        return inflater.inflate(R.layout.fragment_show_products, container, false).apply {

            recyclerView = this.findViewById<RecyclerView>(R.id.recyclerView_show_list).apply {
                adapter = showProductsListAdapter
                layoutManager = GridLayoutManager(this.context, 2)
            }
        }
    }

     override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState)
         val bundle = this.arguments
         bundle?.let {
            items = bundle.getParcelableArrayList("secondMenuItem")

         }
     }
    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is ShowProductsFragment.OnShowFragmentInteractionListener) {
            listener = context
        }
    }
    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    val mListItemListener = object : ShowProductsListAdapter.OnCellProductItemClickListener{
        override fun onShowItemClick(item: Product) {
            listener?.onShowFragmentInteraction(item)
        }
    }

    // Funcion para instanciar el fragment
    companion object {
        fun newInstance(page: Int, title: String): ShowProductsFragment {
            val showProductsFragment = ShowProductsFragment()
            val args = Bundle()
            args.putInt("someInt", page)
            args.putString("someTitle", title)
            showProductsFragment.arguments = args
            return showProductsFragment
        }
    }

}