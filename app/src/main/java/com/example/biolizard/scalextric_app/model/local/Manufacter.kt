package com.example.biolizard.scalextric_app.model.local

enum class Manufacter {
    TECNITOYS, NINCO, CARRERA, EXIN, FLY, HORNBY
}