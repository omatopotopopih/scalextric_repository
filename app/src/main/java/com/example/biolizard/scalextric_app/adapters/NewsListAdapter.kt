package com.example.biolizard.scalextric_app.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.biolizard.scalextric_app.R
import com.example.biolizard.scalextric_app.model.local.Product
import kotlinx.android.synthetic.main.news_list_cell_layout.view.*

class NewsListAdapter(private val items: ArrayList<Product>,
                      private val mListenerNews: OnNewsCellItemClickListener? = null) : RecyclerView.Adapter<NewsListAdapter.NewsListAdapterViewHolder>() {

    // Define the listener interface
    interface OnNewsCellItemClickListener {
        fun onItemClick(item: Product)
    }

    override fun onBindViewHolder(holder: NewsListAdapterViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            mListenerNews?.onItemClick(items[position])
        }
        holder.setValues(items[position])
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsListAdapterViewHolder {
        // Create a new view
        return NewsListAdapterViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.news_list_cell_layout,
                parent,
                false
            )
        )
    }

    inner class NewsListAdapterViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        private val nameTextView = view.findViewById<TextView>(R.id.textView_name)
        fun setValues(product: Product) {
            nameTextView.text = product.name
            itemView.image_news.setImageResource(product.photo)
        }

    }
}
