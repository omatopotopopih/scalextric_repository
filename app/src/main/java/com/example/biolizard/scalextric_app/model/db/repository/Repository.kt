package com.example.biolizard.scalextric_app.model.db.repository

import android.app.Application
import com.example.biolizard.scalextric_app.model.db.DB
import com.example.biolizard.scalextric_app.model.db.daos.*
import com.example.biolizard.scalextric_app.model.local.Accesory
import com.example.biolizard.scalextric_app.model.local.Car
import com.example.biolizard.scalextric_app.model.local.Circuit
import com.example.biolizard.scalextric_app.model.local.Product

class Repository(application: Application) {

    lateinit var carDao: CarDao
    lateinit var accesoryDao: AccesoryDao
    lateinit var circuitDao: CircuitDao
    lateinit var circuitToCarDao: CircuitToCarDao
    lateinit var circuitToAccesoryDao: CircuitToAccesoryDao

    companion object {
        @Volatile
        private var INSTANCE: Repository? = null

        fun getInstance(context: Application): Repository =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: Repository(context).also { INSTANCE = it }
            }
    }

    init {
        val db = DB.getDatabase(application)
        db?.let {
            this.carDao = db.carDao
            this.circuitDao = db.circuitDao
            this.accesoryDao = db.accesoryDao
            this.circuitToAccesoryDao = db.circuitToAccesoryDao
            this.circuitToCarDao = db.circuitToCarDao
        }
    }

    fun getAllCars(): List<Car> = carDao.getAllCars().map { Car(it) }

    fun getAllCircuits(): ArrayList<Circuit> {
        val localCircuitData = ArrayList<Circuit>()

        circuitDao.getAllCircuits().forEach { entityCircuit ->
            val tmpList = ArrayList<Product>()
            entityCircuit.id?.let { id ->
                circuitToCarDao.getAllCarsFromCircuit(id).forEach {
                    tmpList.add(Car(carDao.getCarFromId(it.carId)))
                }

                circuitToAccesoryDao.getAllAccesoriesFromCircuit(id).forEach {
                    tmpList.add(Accesory(accesoryDao.getAccesoryFromId(it.accesoryId)))
                }
                localCircuitData.add(Circuit(entityCircuit, tmpList))
            }
        }
        return localCircuitData
    }

    fun getAllAccesory() = accesoryDao.getAllAccesories().map { Accesory(it) }
}