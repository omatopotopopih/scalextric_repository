package com.example.biolizard.scalextric_app.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.VideoView
import com.example.biolizard.scalextric_app.R
import android.support.v4.media.session.MediaControllerCompat.setMediaController
import android.widget.MediaController
import android.media.MediaPlayer
import android.media.MediaPlayer.OnPreparedListener






class HomeFragment : Fragment() {

    private var listener: OnHomeFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // setVideoBackground()


    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_home, container, false)
        val videoView = rootView?.findViewById<VideoView>(R.id.video_view)
        videoView?.let {
            setvideo(it)
        }
        return rootView
    }
    // Funcion que carga el video de la carpeta raw a nuestro videoView
    fun setvideo(videoView: VideoView){
        val uri = Uri.parse("android.resource://" + activity?.packageName + "/" + R.raw.scalextric_video)
        videoView.setVideoURI(uri)
        videoView.setOnPreparedListener { mp -> mp.isLooping = true; mp.setVolume(0f, 0f )}
        videoView.start()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnHomeFragmentInteractionListener) {
            listener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    interface OnHomeFragmentInteractionListener {
        fun onHomeFragmentInteractionListener(uri: Uri)
    }
    // Funcion para instanciar el fragment.
    companion object {
        fun newInstance(page: Int, title: String): HomeFragment {
            val homeFragment = HomeFragment()
            val args = Bundle()
            args.putInt("someInt", page)
            args.putString("someTitle", title)
            homeFragment.arguments = args
            return homeFragment
        }
    }
}
