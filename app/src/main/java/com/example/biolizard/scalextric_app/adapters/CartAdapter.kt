package com.example.biolizard.scalextric_app.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.example.biolizard.scalextric_app.R
import com.example.biolizard.scalextric_app.model.local.Cart
import java.util.*

class CartAdapter : RecyclerView.Adapter<CartAdapter.MyViewHolder>() {

    private val mDataList = ArrayList(Cart.selectedProducts.keys)
    private var totalPrice = 0.0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_product, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val tmpProduct = mDataList[position]
        holder.name.text = tmpProduct.name
        holder.desc.text = tmpProduct.description
        Cart.selectedProducts[tmpProduct]?.let { value ->

            val tmpPrice = tmpProduct.price * value
            this.totalPrice += tmpPrice
            holder.price.text = "${tmpPrice}€"
            holder.btnRemove.setOnClickListener {
                val tmpValue = value - 1
                if (tmpValue <= 0) {
                    Cart.selectedProducts.remove(tmpProduct)
                } else {
                    Cart.selectedProducts[tmpProduct] = tmpValue
                }
                this.notifyItemChanged(position)
            }
        }
        holder.img.setImageResource(tmpProduct.photo)

    }

    override fun getItemCount(): Int {
        return mDataList.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var name: TextView = itemView.findViewById(R.id.name_product)
        internal var desc: TextView = itemView.findViewById(R.id.desc_product)
        internal var price: TextView = itemView.findViewById(R.id.price_product)
        internal var img: ImageView = itemView.findViewById(R.id.img_product)
        internal var btnRemove: ImageButton = itemView.findViewById(R.id.rmvBtn)
    }
}
