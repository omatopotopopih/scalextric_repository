package com.example.biolizard.scalextric_app.model.db.daos

import com.example.biolizard.scalextric_app.model.db.entities.EntityCar
import android.arch.persistence.room.*


@Dao
interface CarDao {

    @Insert
    fun insertCars(entityCars: List<EntityCar>)

    @Query("DELETE FROM CAR_TABLE")
    fun deleteCars()

    @Delete
    fun deleteCars(entityCars: List<EntityCar>)

    @Update
    fun updateCars(entityCars: List<EntityCar>)

    @Query("SELECT * from car_table ORDER BY id ASC")
    fun getAllCars(): List<EntityCar>

    @Query("SELECT * from car_table WHERE id = :carId LIMIT 1")
    fun getCarFromId(carId: Int): EntityCar

}