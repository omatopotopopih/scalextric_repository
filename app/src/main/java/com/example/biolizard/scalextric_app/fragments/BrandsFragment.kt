package com.example.biolizard.scalextric_app.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Toast

import com.example.biolizard.scalextric_app.R
import kotlinx.android.synthetic.main.fragment_brands.*


class BrandsFragment : Fragment() {
    private var btnFilterCarrera: ImageButton? = null
    private var btnFilterExin: ImageButton? = null
    private var btnFilterTecnitoys: ImageButton? = null
    private var btnFilterFly: ImageButton? = null
    private var btnFilterHornby: ImageButton? = null
    private var btnFilterNinco: ImageButton? = null

    companion object {

        fun newInstance(page: Int, title: String): BrandsFragment {
            val brandsFragment = BrandsFragment()
            val args = Bundle()
            args.putInt("someInt", page)
            args.putString("someTitle", title)
            brandsFragment.arguments = args
            return brandsFragment
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_brands, container, false)
        findButtonID(rootView)
        imgButtonsListener(rootView)


        return rootView
    }

    private fun imgButtonsListener(rootView: View) {
        btnFilterCarrera?.setOnClickListener {
            Toast.makeText(rootView.context, getString(R.string.stringCarrera), Toast.LENGTH_SHORT).show()
            txt_filter.text = getString(R.string.stringCarrera)
        }

        btnFilterExin?.setOnClickListener {
            Toast.makeText(rootView.context, getString(R.string.stringExin), Toast.LENGTH_SHORT).show()
            txt_filter.text = getString(R.string.stringExin)
        }

        btnFilterTecnitoys?.setOnClickListener {
            Toast.makeText(rootView.context, getString(R.string.stringTecnitoys), Toast.LENGTH_SHORT).show()
            txt_filter.text = getString(R.string.stringTecnitoys)
        }

        btnFilterFly?.setOnClickListener {
            Toast.makeText(rootView.context, getString(R.string.stringFly), Toast.LENGTH_SHORT).show()
            txt_filter.text = getString(R.string.stringFly)
        }

        btnFilterNinco?.setOnClickListener {
            Toast.makeText(rootView.context, getString(R.string.stringNinco), Toast.LENGTH_SHORT).show()
            txt_filter.text = getString(R.string.stringNinco)
        }

        btnFilterHornby?.setOnClickListener {
            Toast.makeText(rootView.context, getString(R.string.stringHornby), Toast.LENGTH_SHORT).show()
            txt_filter.text = getString(R.string.stringHornby)
        }
    }


    private fun findButtonID(rootView: View){
        btnFilterCarrera = rootView.findViewById(R.id.btn_filter_carrera)
        btnFilterExin = rootView.findViewById(R.id.btn_filter_exin)
        btnFilterTecnitoys = rootView.findViewById(R.id.btn_filter_tecnitoys)
        btnFilterFly = rootView.findViewById(R.id.btn_filter_fly)
        btnFilterNinco = rootView.findViewById(R.id.btn_filter_ninco)
        btnFilterHornby = rootView.findViewById(R.id.btn_filter_hornby)
    }

}
