package com.example.biolizard.scalextric_app.model.db.daos

import android.arch.persistence.room.*
import com.example.biolizard.scalextric_app.model.db.entities.CircuitToCar

@Dao
interface CircuitToCarDao {
    @Insert
    fun insertCircuitToCar(data: List<CircuitToCar>)

    @Delete
    fun deleteCircuitToCar(entityAccesories: List<CircuitToCar>)

    @Update
    fun updateCircuitToCar(entityAccesories: List<CircuitToCar>)

    @Query("SELECT * from CIRCUIT_TO_CAR_TABLE WHERE circuitId = :id")
    fun getAllCarsFromCircuit(id:Int): List<CircuitToCar>

    @Query("SELECT * from CIRCUIT_TO_CAR_TABLE WHERE carId = :id")
    fun getAllCircuitsFromCar(id:Int): List<CircuitToCar>
}