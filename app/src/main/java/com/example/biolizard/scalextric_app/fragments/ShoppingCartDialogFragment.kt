package com.example.biolizard.scalextric_app.fragments

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.example.biolizard.scalextric_app.R
import com.example.biolizard.scalextric_app.adapters.CartAdapter
import com.example.biolizard.scalextric_app.model.local.Cart
import kotlinx.android.synthetic.main.fragment_shopping_cart_dialog.view.*


class ShoppingCartDialogFragment : DialogFragment() {
    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: RecyclerView.Adapter<*>? = null

    companion object {
        fun newInstance() = ShoppingCartDialogFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_shopping_cart_dialog, container, false)
        v.btn_close_cart.setOnClickListener {
            dialog.dismiss()
        }
        v.apply {
            findViewById<RecyclerView>(R.id.recycler_view_cart).apply {
                layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
                adapter = CartAdapter()
                adapter?.notifyDataSetChanged()

            }
        }
        v.final_price.text = Cart.getTotalPrice().toString()

        v.buy_button.setOnClickListener {
            val tmpDataCart = Cart.selectedProducts
            if (tmpDataCart.size > 0) {
                if (tmpDataCart.size == 1) {
                    Toast.makeText(
                        view?.context,
                        "Has comprado " + Cart.selectedProducts.size + " producto",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    Toast.makeText(
                        view?.context,
                        "Has comprado " + Cart.selectedProducts.size + " productos",
                        Toast.LENGTH_LONG
                    ).show()
                }
                Cart.selectedProducts.clear()
            }
            dialog.dismiss()
        }

        return v
    }


}