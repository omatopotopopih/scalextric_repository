package com.example.biolizard.scalextric_app.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.biolizard.scalextric_app.R
import com.example.biolizard.scalextric_app.model.local.Category
import kotlinx.android.synthetic.main.main_list_cell_layout.view.*
import java.util.*

class MainListAdapter(
    private val categories: ArrayList<Category>,
    val mListener: OnCellItemClickListener? = null
) : RecyclerView.Adapter<MainListAdapter.ViewHolder>() {

    // Define the listener interface
    interface OnCellItemClickListener {
        fun onItemClick(item: Category)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            mListener?.onItemClick(categories[position])
        }
        holder.setValues(categories[position])
    }

    override fun getItemCount(): Int {
        return categories.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.main_list_cell_layout, parent, false))
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        private val nameTextView = view.findViewById<TextView>(R.id.textView_name)

        fun setValues(category: Category) {
            nameTextView.text = category.categoryType.toString()
            itemView.imageCategory.setImageResource(category.photo)
        }

    }
}
