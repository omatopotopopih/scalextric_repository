package com.example.biolizard.scalextric_app.model.db.converters

import android.arch.persistence.room.TypeConverter
import com.example.biolizard.scalextric_app.model.local.Category
import com.example.biolizard.scalextric_app.model.local.CategoryEnum
import com.google.gson.Gson

object ConverterCategory {

    @TypeConverter
    @JvmStatic
    fun manufacterToString(category: CategoryEnum): String {
        return Gson().toJson(category)
    }


    @TypeConverter
    @JvmStatic
    fun stringToManufacter(categoryString: String): CategoryEnum? =
        Gson().fromJson(categoryString, CategoryEnum::class.java)

}
