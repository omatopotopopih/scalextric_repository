package com.example.biolizard.scalextric_app.model.local

import com.example.biolizard.scalextric_app.R

class Category (var categoryType: CategoryEnum){

    var photo:Int = -1

    init {
        when(this.categoryType){
            CategoryEnum.RALLY -> this.photo = R.drawable.rally

            CategoryEnum.DTM -> this.photo = R.drawable.dtm

            CategoryEnum.F1 -> this.photo = R.drawable.f1
        }
    }

}


enum class CategoryEnum{
    RALLY, DTM, F1
}