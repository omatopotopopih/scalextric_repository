package com.example.biolizard.scalextric_app.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.biolizard.scalextric_app.R
import com.example.biolizard.scalextric_app.model.local.Product
import kotlinx.android.synthetic.main.main_list_cell_layout.view.*

class ShowProductsListAdapter(private val items: ArrayList<Product>, val mListener: OnCellProductItemClickListener? = null): RecyclerView.Adapter<ShowProductsListAdapter.ShowViewHolder>() {
    // Define listener member variable

    // Define the listener interface
    interface OnCellProductItemClickListener {
        fun onShowItemClick(item: Product)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowViewHolder {

        return ShowViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.products_list_cell_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: ShowViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            mListener?.onShowItemClick(items[position])
        }
        holder.setValues(items[position])    }


    inner class ShowViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        private val nameTextView = view.findViewById<TextView>(R.id.textView_name)
        fun setValues(product: Product){
            nameTextView.text = product.name
            itemView.imageCategory.setImageResource(product.photo)

        }

    }
}