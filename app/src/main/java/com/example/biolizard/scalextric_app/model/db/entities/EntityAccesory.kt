package com.example.biolizard.scalextric_app.model.db.entities;

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.example.biolizard.scalextric_app.model.local.CategoryEnum
import com.example.biolizard.scalextric_app.model.local.Manufacter

@Entity(tableName = "ACCESORY_TABLE")
class EntityAccesory(
    var name: String,
    var brand: String,
    var price: Double,
    var description: String,
    var manufacter: Manufacter,
    var photo: Int,
    var category: CategoryEnum
) {

    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
}